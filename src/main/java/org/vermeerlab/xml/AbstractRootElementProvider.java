/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.xml;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * XMLのルートElementを取得する機能を提供する抽象クラスです.
 *
 * @author Yamashita,Takahiro
 */
public abstract class AbstractRootElementProvider {

    /**
     * XMLのルートElementを返却します.
     *
     * @param path ルートElementを取得するパス
     * @return ルートElement
     */
    public Element apply(String path) {
        try (InputStream inputStream = this.inputStream(path)) {
            return DocumentBuilderFactory
                    .newInstance()
                    .newDocumentBuilder()
                    .parse(inputStream)
                    .getDocumentElement();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            throw new XmlReaderException(ex, "XML file can not get. param path is ", "''", path, "''");
        }
    }

    /**
     * 資産の{@link java.io.InputStream} を返却します.
     *
     * @param path 取得資産のpath
     * @return 取得した資産の{@link java.io.InputStream}
     */
    protected abstract InputStream inputStream(String path);

}
