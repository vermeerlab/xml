/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * XMLの参照を行います.
 *
 * @author Yamashita,Takahiro
 */
public class XmlReader {

    final Element rootElement;

    /**
     * for extention
     */
    protected XmlReader() {
        this.rootElement = null;
    }

    protected XmlReader(Element element) {
        this.rootElement = element;
    }

    /**
     * クラスパスにあるXMLファイルを参照してインスタンスを構築します.
     * <P>
     * クラスパスの指定なので、{@code /} から指定してください.<br>
     * 例：{@literal src/main/resources/hoge.xml} を指定する場合：{@literal "/hoge.xml"}
     *
     * @param classPath 参照先のXMLのクラスパス
     * @return 構築したインスタンス
     * @throws XmlReaderException リソースの取得が出来ない、または定義不正で取得できなかった場合
     */
    public static XmlReader ofClassPath(String classPath) {
        Element element = new ClassPathRootElementProvider().apply(classPath);
        return new XmlReader(element);
    }

    /**
     * XMLファイルを参照してインスタンスを構築します.
     *
     * @param filePath 参照先のXMLのファイルパス
     * @return 構築したインスタンス
     * @throws XmlReaderException リソースの取得が出来ない、または定義不正で取得できなかった場合
     */
    public static XmlReader ofFilePath(String filePath) {
        Element element = new FilePathRootElementProvider().apply(filePath);
        return new XmlReader(element);
    }

    /**
     * xpath指定により要素値を取得して返却します.
     * <P>
     * 要素が無い、および 要素の値が空文字も場合はリストの対象外とします.
     *
     * @param conditionXpath 検索条件のxpath記述
     * @return 取得した要素値リスト
     * @throws XmlReaderException Xpath解析時に異常があった場合
     */
    public List<String> scanTextValue(String conditionXpath) {
        NodeList nodeList = this.toXPathExpression(conditionXpath);

        List<String> results = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            String result = this.formatNodeValue(nodeList.item(i));
            if (Objects.equals(result, "") == false) {
                results.add(result);
            }
        }
        return Collections.unmodifiableList(results);
    }

    /**
     * xpath指定したノードが存在するか判定します.
     *
     * @param conditionXpath 検索条件のxpath記述
     * @return ノードが存在する場合 true
     * @throws XmlReaderException Xpath解析時に異常があった場合
     */
    public Boolean hasNode(String conditionXpath) {
        NodeList nodeList = this.toXPathExpression(conditionXpath);
        return 0 < nodeList.getLength();
    }

    NodeList toXPathExpression(String conditionXpath) {
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        Document document = this.rootElement.getOwnerDocument();
        XPathExpression xPathExpression;
        try {
            xPathExpression = xpath.compile(conditionXpath);
            NodeList nodeList = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);
            return nodeList;
        } catch (XPathExpressionException ex) {
            throw new XmlReaderException(ex, "XPATH can not evaluate. param xpath is ", "''", conditionXpath, "''");
        }
    }

    /**
     * ノート値を整形して返却します.
     *
     * @param node
     * @return 整形後のノード値
     */
    String formatNodeValue(Node node) {
        return node.getNodeType() == Node.CDATA_SECTION_NODE
               ? node.getNodeValue()
               : node.getNodeValue().replaceAll("\r|\n|\\s", "");
    }
}
