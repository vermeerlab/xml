/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.xml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * FilePathを指定してXMLのルートElementを取得する機能を提供します.
 *
 * @author Yamashita,Takahiro
 */
class FilePathRootElementProvider extends AbstractRootElementProvider {

    /**
     * {@inheritDoc }
     */
    @Override
    protected InputStream inputStream(String filePath) {
        try {
            return new FileInputStream(filePath);
        } catch (FileNotFoundException ex) {
            throw new XmlReaderException(ex, "Xml file does not find. param = " + filePath);
        }
    }
}
