/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.xml;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Test;

public class XmlReaderExceptionTest {

    @Test
    public void 例外処理のインスタンス化() {
        XmlReaderException ex = new XmlReaderException();
        Assert.assertThat(ex, instanceOf(XmlReaderException.class));
    }

    @Test
    public void 例外処理のメッセージ引数あり() {
        XmlReaderException ex = new XmlReaderException("Test1", "Test2");
        Assert.assertThat(ex.getMessage().replaceAll("\r|\n", ""), is("Test1Test2"));
    }

    @Test
    public void 例外処理のメッセージ引数あり_例外() {
        Exception _ex = new RuntimeException("Test");
        XmlReaderException ex = new XmlReaderException(_ex, "Test1", "Test2");
        Assert.assertThat(ex.getMessage().replaceAll("\r|\n", "").contains("Test1Test2"), is(true));
    }
}
