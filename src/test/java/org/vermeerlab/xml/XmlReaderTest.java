/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.xml;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * XMLの形式不正はデフォルトでは標準エラーへ出力される。
 * テスト中は表示確認は不要なのでバッファーで処理をする。
 *
 * @author Yamashita ,Takahiro
 */
public class XmlReaderTest {

    private ByteArrayOutputStream _baos;
    private PrintStream _out;

    @Before
    public void setUp() throws FileNotFoundException {
        _baos = new ByteArrayOutputStream();
        _out = System.err;
        System.setErr(
                new PrintStream(
                        new BufferedOutputStream(_baos)
                )
        );
    }

    @After public void tearDown() {
        System.setErr(_out);
    }

    @Test
    public void 正しい書式のClassPathXML読み込み() {
        XmlReader xmlReader = XmlReader.ofClassPath("/org/vermeerlab/xml/ok.xml");
        List<String> results = xmlReader.scanTextValue("//main/item/text()");
        Assert.assertThat(results.size(), is(2));
    }

    @Test
    public void 正しい書式のFilePathXML読み込み() {
        String filePath = new File(this.getClass()
                .getResource("/org/vermeerlab/xml/ok.xml")
                .getPath())
                .getAbsolutePath();
        XmlReader xmlReader = XmlReader.ofFilePath(filePath);
        List<String> results = xmlReader.scanTextValue("//main/item/text()");
        Assert.assertThat(results.size(), is(2));
    }

    @Test
    public void 親要素が存在しない() {
        XmlReader xmlReader = XmlReader.ofClassPath("/org/vermeerlab/xml/childNodeValues.xml");
        List<String> results = xmlReader.scanTextValue("//notParent/item/text()");
        Assert.assertThat(results.size(), is(0));
    }

    @Test
    public void 親要素が空() {
        XmlReader xmlReader = XmlReader.ofClassPath("/org/vermeerlab/xml/childNodeValues.xml");
        List<String> results = xmlReader.scanTextValue("//main/item/text()");
        Assert.assertThat(results.size(), is(0));
    }

    @Test
    public void CDATAの読み込み() {
        XmlReader xmlReader = XmlReader.ofClassPath("/org/vermeerlab/xml/cdata.xml");
        List<String> results = xmlReader.scanTextValue("//main/item/text()");
        String result = results.iterator().next();
        Assert.assertEquals("\n1\n2\n3\n", result);
    }

    @Test
    public void 親ノードの属性値() {
        XmlReader xmlReader = XmlReader.ofClassPath("/org/vermeerlab/xml/ok_attribute.xml");
        List<String> results = xmlReader.scanTextValue("root/main/item/text()");
        Assert.assertThat(results.size(), is(2));
        Assert.assertThat(results.toString(), is("[OK１, OK２]"));
    }

    @Test
    public void 親ノードの属性値_異常() {
        XmlReader xmlReader = XmlReader.ofClassPath("/org/vermeerlab/xml/ng_attribute.xml");
        List<String> results = xmlReader.scanTextValue("//main[@value='mainAttribute']/item/text()");
        Assert.assertThat(results.size(), is(0));
    }

    @Test
    public void 親ノードの属性値_異常_NULL() {
        XmlReader xmlReader = XmlReader.ofClassPath("/org/vermeerlab/xml/ng_attribute_null.xml");
        List<String> results = xmlReader.scanTextValue("//main[@value='mainAttribute']/item/text()");
        Assert.assertThat(results.size(), is(0));
    }

    @Test
    public void 子要素が存在しない() {
        XmlReader xmlReader = XmlReader.ofClassPath("/org/vermeerlab/xml/nochild.xml");
        List<String> results = xmlReader.scanTextValue("//main/item/text()");
        Assert.assertThat(results.size(), is(0));
    }

    @Test(expected = XmlReaderException.class)
    public void Xpathの指定が正しくない() {
        XmlReader xmlReader = XmlReader.ofClassPath("/org/vermeerlab/xml/nochild.xml");
        List<String> results = xmlReader.scanTextValue("//main/.node()");
    }

    @Test
    public void 属性値がないものだけを取得する() {
        XmlReader xmlReader = XmlReader.ofClassPath("/org/vermeerlab/xml/ok_attribute_1.xml");
        List<String> results = xmlReader.scanTextValue(
                "//main/item[not(@value)]/text()");
        Assert.assertThat(results.toString(), is("[OK２]"));
    }

    @Test
    public void 属性値を取得する() {
        XmlReader xmlReader = XmlReader.ofClassPath("/org/vermeerlab/xml/ok_attribute_2.xml");
        List<String> results = xmlReader.scanTextValue(
                "//main/item[@value]/@value");
        Assert.assertThat(results.toString(), is("[itemAttribute1, itemAttribute2]"));
    }

    @Test
    public void ノードの存在確認() {
        XmlReader xmlReader = XmlReader.ofClassPath("/org/vermeerlab/xml/ok_attribute_2.xml");
        Boolean result = xmlReader.hasNode("//main/item[@value]/@value");
        Assert.assertThat(result, is(true));
    }

    @Test
    public void コンストラクタカバレッジ() {
        XmlReader xmlReader = new XmlReader();
    }

}
