/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.xml;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * XMLの形式不正はデフォルトでは標準エラーへ出力される。
 * テスト中は表示確認は不要なのでバッファーで処理をする。
 *
 * @author Yamashita ,Takahiro
 */
public class AbstractRootElementProviderTest {

    private ByteArrayOutputStream _baos;
    private PrintStream _out;

    @Before
    public void setUp() throws FileNotFoundException {
        _baos = new ByteArrayOutputStream();
        _out = System.err;
        System.setErr(
                new PrintStream(
                        new BufferedOutputStream(_baos)
                )
        );
    }

    @After public void tearDown() {
        System.setErr(_out);
    }

    @Test(expected = XmlReaderException.class)
    public void 資産取得時に例外() {
        AbstractRootElementProvider provider = new AbstractRootElementProvider() {
            @Override
            protected InputStream inputStream(String path) {
                InputStream stream = new InputStream() {
                    @Override
                    public int read() throws IOException {
                        throw new IOException();
                    }
                };
                return stream;
            }
        };
        provider.apply("");
    }

}
